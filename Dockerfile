FROM tiangolo/uwsgi-nginx-flask:python3.7

ENV STATIC_PATH /app/app/static

COPY ./app /app
RUN pip install -r /app/app/requirements.txt
