from . import config


def read_day_entry(dateparam):
    day_entry = config.db().days.find_one({"day": dateparam})
    return day_entry


def read_edition(dateparam):
    return config.db().editions.find_one({"days": dateparam})


def read_position(id):
    pos = config.db().positions.find_one({"id": int(id)})
    return pos


def save(position):
    config.db().positions.find_one_and_replace({"id": position["id"]}, position)


def save_day_entry(day_entry):
    config.db().days.find_one_and_replace({"day": day_entry["day"]}, day_entry)


def solved_main_count():
    return config.db().positions.count({"main": True, "played_move": {"$ne": None}})


def read_user(username):
    user = config.db().users.find_one({"name": username})
    return user
