from flask import Blueprint, render_template, request, session, redirect, url_for
import bcrypt

from . import config
from . import logic


bp = Blueprint("auth", __name__)


@bp.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form["username"]
        encoded_password = str.encode(request.form["password"])
        if _auth(username, encoded_password):
            session["username"] = username
            return redirect(url_for("training.index"))
        else:
            return "Not logged in."

    return _render_template_with_current_edition("login.html")


def _auth(username, encoded_password):
    user = logic.read_user(username)
    hashed = str.encode(user["password"])

    return bcrypt.checkpw(encoded_password, hashed)


@bp.route("/logout")
def logout():
    session.pop("username", None)
    return redirect(url_for("training.index"))


def _render_template_with_current_edition(template):
    current_edition = logic.read_edition(config.current_day)
    return render_template(
        template,
        background_img=current_edition["background-img"],
        edition_title=current_edition["title"],
    )
