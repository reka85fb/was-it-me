import bson
import datetime

from . import storage


def read_day_entry(dateparam):
    day_entry = storage.read_day_entry(dateparam)
    if not day_entry:
        return None

    if "guessed_topic" not in day_entry.keys():
        day_entry["guessed_topic"] = ""

    return day_entry


def read_edition(dateparam):
    return storage.read_edition(dateparam)


def read_position(id):
    pos = storage.read_position(id)

    pos["done"] = is_attempted(pos)
    pos["feedback_text"] = _calculate_was_it_me_feedback_text(pos)
    pos["move_feedback"] = _calculate_move_feedback(pos)
    _load_related_positions(pos)
    return pos


def save(position):
    storage.save(position)


def solved_main_count():
    return storage.solved_main_count()


def add_attempt(position_id, move, was_it_me):
    position = read_position(position_id)

    if not was_it_me and position["main"] and len(position["attempts"]) == 0:
        return False

    now_timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    current_attempt = {
        "move": move,
        "timestamp": now_timestamp,
        "id": bson.objectid.ObjectId(),
        "was_it_me": was_it_me,
    }

    position["attempts"].append(current_attempt)

    if was_it_me:
        position["was_it_me_guess"] = was_it_me

    position["played_move"] = move
    position["played_move_good"] = _calculate_move_good(move, position["best_move"])

    if len(position["attempts"]) == 1:
        position["first_played_move_good"] = position["played_move_good"]

    save(position)
    return True


def guess_topic(topic, day):
    day_entry = read_day_entry(day)
    day_entry["guessed_topic"] = topic
    storage.save_day_entry(day_entry)


def is_attempted(position):
    return len(position["attempts"]) > 0


def read_user(username):
    return storage.read_user(username)


def _load_related_positions(position):
    position["id"] = int(position["id"])
    position["move_index"] = int(position["move_index"])

    if (
        "related_positions" in position.keys()
        and "follow_up" in position["related_positions"].keys()
    ):
        result = []
        for fup_pos_id in position["related_positions"]["follow_up"]:
            fup_pos = read_position(fup_pos_id)

            result.append(fup_pos)
        position["follow_up_positions"] = result

    if (
        "related_positions" in position.keys()
        and "hints" in position["related_positions"].keys()
    ):
        result = []
        for hint_pos_id in position["related_positions"]["hints"]:
            hint_pos = read_position(hint_pos_id)

            result.append(hint_pos)
        position["hint_positions"] = result


def _calculate_was_it_me_feedback_text(position):
    if not is_attempted(position):
        return ""

    if "was_it_me_guess" not in position.keys():
        return ""

    real = position["was_it_me"] == "yes"
    said = position["was_it_me_guess"] == "yes"

    if real and said:
        return "Yes, it was YOU indeed."
    if real and not said:
        return "Believe it or not: It was YOU."
    if not real and said:
        return "Actually, it wasn't YOU."
    if not real and not said:
        return "Yep. It really wasn't YOU."


def _calculate_move_good(played, best):
    p = _simplify_move(played)
    b = _simplify_move(best)
    if isinstance(b, str):
        return p == b
    if isinstance(b, list):
        return p in b
    return False


def _simplify_move(move):
    if isinstance(move, str):
        return move.replace("x", "").replace("+", "")
    if isinstance(move, list):
        return [m.replace("x", "").replace("+", "") for m in move]
    return move


def _calculate_move_feedback(position):
    if not is_attempted(position):
        return

    if position["played_move_good"]:
        return "Congrats, this is the best move."
    return "There's a better move here."
