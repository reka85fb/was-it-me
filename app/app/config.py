import pymongo
import os

current_day = "2019-08-18"


def db():
    db_host = os.environ.get("WAS_IT_ME_DB_HOST")
    db_name = os.environ.get("WAS_IT_ME_DB")
    client = pymongo.MongoClient(db_host, 27017)
    return client[db_name]
