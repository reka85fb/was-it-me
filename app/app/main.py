from flask import Flask
import os

from . import training
from . import auth


def create_app():
    app = Flask(__name__)
    app.secret_key = str.encode(os.environ.get("SECRET_KEY"))
    app.register_blueprint(training.bp)
    app.register_blueprint(auth.bp)
    return app


app = create_app()
