from flask import Blueprint, render_template, request, session, redirect, url_for
import datetime

from . import config
from . import logic


bp = Blueprint("training", __name__)


@bp.route("/")
def index():
    return days(config.current_day)


@bp.route("/positions/<int:id>/attempts", methods=["POST"])
def play(id):
    if "username" not in session:
        return "Not logged in."

    move = request.form["move"]
    if not _validate(move):
        return _render_template_with_current_edition("invalid_move_page.html")

    day = request.form["day"]

    if "was_it_me" in request.form.keys():
        was_it_me = request.form["was_it_me"]
    else:
        was_it_me = None

    result = logic.add_attempt(id, move, was_it_me)

    if not result:
        return _render_template_with_current_edition("no_was_it_me_input.html")

    day_url = url_for("training.days", dateparam=day)
    return redirect(day_url + "#content")


@bp.route("/topic", methods=["POST"])
def topic():
    if "username" not in session:
        return "Not logged in."

    topic = request.form["topic"]
    day = request.form["day"]

    logic.guess_topic(topic, day)

    return redirect(url_for("training.index"))


@bp.route("/days/<string:dateparam>")
def days(dateparam):
    logged_in = "username" in session

    solved_main_count = logic.solved_main_count()

    day_entry = logic.read_day_entry(dateparam)
    if not day_entry:
        return _render_template_with_current_edition("invalid_page.html")

    dateparam_value = datetime.datetime.strptime(dateparam, "%Y-%m-%d")
    if dateparam != config.current_day and dateparam_value > datetime.datetime.today():
        return _render_template_with_current_edition("invalid_page.html")

    position_ids = day_entry["positions"]
    positions = []
    for p in position_ids:
        position = logic.read_position(p)

        positions.append(position)

    both_attempted = logic.is_attempted(positions[0]) and logic.is_attempted(
        positions[1]
    )

    edition = logic.read_edition(dateparam)
    slug = edition["slug"]

    return render_template(
        f"editions/{slug}.html",
        positions=positions,
        logged_in=logged_in,
        both_attempted=both_attempted,
        guessed_topic=day_entry["guessed_topic"],
        dateparam=dateparam,
        solved_main_count=solved_main_count,
        background_img=edition["background-img"],
        edition_title=edition["title"],
    )


def _validate(move):
    if len(move) < 2 or len(move) > 8:
        return False
    return True


def _render_template_with_current_edition(template):
    current_edition = logic.read_edition(config.current_day)
    return render_template(
        template,
        background_img=current_edition["background-img"],
        edition_title=current_edition["title"],
    )
